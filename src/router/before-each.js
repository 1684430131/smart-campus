import {
    getToken
} from '../utils'

/**
 * @description 路由导航首位
 */
export const beforeEach = function (router) {
    router.beforeEach(function (to, _, next) {

        if (to.name == "login") {
            next()
        } else {
            if (!getToken()) {

                next({
                    name: "login"
                })
            } else {
                next()
            }
        }

    })

}