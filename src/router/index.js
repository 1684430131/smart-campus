
import Vue from 'vue'
import VueRouter from 'vue-router'
import {routes} from "./routes"
import {beforeEach} from "./before-each"
Vue.use(VueRouter)

const router = new VueRouter({
  routes
})

beforeEach(router)

export default router
