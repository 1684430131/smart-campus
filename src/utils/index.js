export const getToken = function () {
    return localStorage.getItem("token")
}

export const setToken = function (token = "1684430131@qq.com") {
    localStorage.setItem("token", token)
}

export const getTime = function () {
    let date = new Date()
    return {
        time: date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(),
        shortTime: date.getHours() + ":" + date.getMinutes(),
        date: date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate(),
        fullTime: date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + " " + date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
    }
}