
import Vue from 'vue'

//app-component
import App from "./app"

//vue-router
import router from "./router"

//components
import "./components"

//design 
import "./design/index.scss"

//element plugin
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(Element)

import vueParticles from "vue-particles"
Vue.use(vueParticles)

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})


