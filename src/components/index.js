import chartBase from "./chart-base"
import systemTitle from "./system-title"
import menu from "./menu"
import vue from "vue"
vue.component("chartBase",chartBase)
vue.component("systemTitle",systemTitle)
vue.component("systemMenu",menu)
